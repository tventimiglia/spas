import json
def handler(context, inputs):
    greeting = "Hello, {0}!".format(inputs["target"])
    print(greeting)

    outputs = {
      "greeting": greeting
    }
    print("=========Context==================================================================")
    print(context)
    print("=========INPUTS==================================================================")
    print(inputs)
    print("=========OUTPUTS==================================================================")
    print(outputs)
    print("=========RESPONSE HEADER DATE==================================================================")
    print(inputs['responseHeaders'])
    print("=========RESPONSE BODY==================================================================")
    print(inputs['responseBody'])
    rb = json.loads(inputs['responseBody'])
    print((json.loads(inputs['responseBody']))["token"])

    return outputs
